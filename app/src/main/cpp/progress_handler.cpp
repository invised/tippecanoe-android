#include <jni.h>
#include "progress_handler.h"

ProgressHandler::ProgressHandler(JNIEnv *env, jobject jObj) {
    _env = env;
    _javaObj = env->NewGlobalRef(jObj);

    jclass java_class = _env->FindClass("com/invised/tippecanoe/Tippecanoe");

    _progressMethod = _env->GetMethodID(java_class, "onProgress", "(D)V");
    _logMethod = _env->GetMethodID(java_class, "printLog", "(Ljava/lang/String;)V");
}

ProgressHandler::~ProgressHandler() {
    _env->DeleteGlobalRef(_javaObj);
}

void ProgressHandler::ReportProgress(double progress) {
    _env->CallVoidMethod(_javaObj, _progressMethod, progress);
}

void ProgressHandler::PrintLog(const char *msg) {

    jstring java_str = _env->NewStringUTF(msg);
    _env->CallVoidMethod(_javaObj, _logMethod, java_str);
}