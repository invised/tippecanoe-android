LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := tippecanoe
LOCAL_SRC_FILES := progress_handler.cpp\
                   jni_bridge.cpp\
                   pool.cpp\
                   sqlite3.c\
                   mbtiles.cpp\
                   serial.cpp\
                   tile.cpp\
                   geometry.cpp\
                   text.cpp\
                   jsonpull/jsonpull.c\
                   geojson.cpp\
                   projection.cpp\
                   memfile.cpp\
                   mvt.cpp\
                   main.cpp\

#APP_STL := c++_shared
APP_CPPFLAGS += -fexceptions
APP_CPPFLAGS += -std=c++11

LOCAL_LDFLAGS += -latomic
LOCAL_LDLIBS := -lz
LOCAL_LDLIBS += -L$(SYSROOT)/usr/lib -llog
include $(BUILD_SHARED_LIBRARY)