#ifndef TIPPECANOE_PROGRESS_HANDLER_H
#define TIPPECANOE_PROGRESS_HANDLER_H

#include <jni.h>

class ProgressHandler {

    private:
        JNIEnv * _env;
        jobject _javaObj;

        jmethodID _progressMethod;
        jmethodID _logMethod;

    public:
        ProgressHandler(JNIEnv *env, jobject jObj);
        ~ProgressHandler();
        
        void ReportProgress(double progress);
        void PrintLog(const char *msg);
};

#endif //TIPPECANOE_PROGRESS_HANDLER_H
