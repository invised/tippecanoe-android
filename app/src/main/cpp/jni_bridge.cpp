#include <jni.h>
#include <android/log.h>
#include <string>
#include <stdio.h>
#include "jni_bridge.hpp"
#include "main.hpp"
#include "progress_handler.h"

ProgressHandler *handler;

extern "C"
JNIEXPORT jint JNICALL
Java_com_invised_tippecanoe_Tippecanoe_execute (
        JNIEnv *env,
        jobject thisObject,
        jobjectArray stringArray) {

    int stringCount = env->GetArrayLength(stringArray);

    char *args[stringCount];

    for (int i = 0; i < stringCount; i++) {
        jstring string = (jstring) (env->GetObjectArrayElement(stringArray, i));
        const char *rawString = env->GetStringUTFChars(string, 0); // make a copy

        args[i] = (char *) rawString;
    }

    handler = new ProgressHandler(env, thisObject);

    int result;
    try {
        result = main(stringCount, args);
    } catch (int code) {
        result = code;
    }

    delete handler;

    for (int i = 0; i < stringCount; i++) {
        jstring string = (jstring) (env->GetObjectArrayElement(stringArray, i));
        env->ReleaseStringUTFChars(string, args[i]);
    }

    return result;
}

void perror_android(const char *msg) {
    handler->PrintLog(msg);
}

void printf_android(FILE *stream, const char *format, ...) {

    char buff[256];

    va_list args;
    va_start(args, format);

    int formatSize = strlen(format);
    int buffSize = formatSize*2;
    char formatted[buffSize];

    vsnprintf (formatted, buffSize, format, args);
    va_end(args);

    perror_android(formatted);
}

void exit_android(int code) {
    throw code;
}
