#ifdef __cplusplus

#include "progress_handler.h"
extern ProgressHandler *handler;

extern "C" {
#endif

    void printf_android(FILE *stream, const char *format, ...);
    void perror_android(const char *msg);
    void exit_android(int code);

#ifdef __cplusplus
}
#endif