package com.invised.tippecanoe;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private Tippecanoe mTippecanoe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTippecanoe = new Tippecanoe();
        int result = mTippecanoe.execute(new String[]{"a", "b"});
        Log.w(TAG, result + "");
    }
}
