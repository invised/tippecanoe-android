package com.invised.tippecanoe;

import android.util.Log;

public class Tippecanoe {
    private static final String TAG = Tippecanoe.class.getSimpleName();

    static {
        System.loadLibrary("tippecanoe");
    }

    public native /*synchronized*/ int execute(String[] args);

    public void printLog(String msg) {
        Log.w(TAG, msg);
    }

    public void onProgress(double progress) {
        printLog("Progress: " + progress);
    }
}
